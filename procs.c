#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {


    pid_t pid, ppid, fpid, wpid;
    int status;
    for(size_t i=0; i<10;++i)
    {
    	fpid = fork();
    	pid = getpid();
        ppid = getppid();

    	if (fpid == -1) {
			printf("Error\n");
		}
		if (fpid == 0) {
			printf("From child %d: My pid = %d, ppid = %d\n", (int)i, (int)pid, (int)ppid);
			status=(int)pid;
			printf("From child %d: Child shutdown \n", (int)i);
			exit(status);
			break;
		}


	}
	printf("From parent: My pid = %d, ppid = %d\n", (int)pid, (int)ppid);
    status=pid;
    for(size_t i=0; i<10;++i)
    {
	    wpid=wait(&status);
	    printf("From parent: Child shutdown with status %d \n",(int)wpid);
	    
	}
    return 0;
}